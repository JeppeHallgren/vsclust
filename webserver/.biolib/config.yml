biolib_version: 2
modules:
    main:
        image: 'local-docker://veitveit/vsclust:latest'
        command: 'python3 main.py'
        working_directory: /home/vsclust/
        input_files:
            - COPY / /home/vsclust/
        output_files:
            - COPY /home/vsclust/ /
        source_files:
            - COPY /main.py /home/vsclust/
            - COPY /sample.csv /home/vsclust/
citation:
    entry_type: article
    author: 'Veit Schwämmle, Ole N Jensen'
    title: 'VSClust: Feature-based variance-sensitive clustering of omics data'
    journal: Bioinformatics
    year: '2018'
    volume: '34'
    doi: 'https://doi.org/10.1093/bioinformatics/bty224'

arguments:
    -
        default_value: sample.csv
        description: Select an input csv file
        key: '--infile'
        key_value_separator: ' '
        required: true
        type: text-file
    -
        default_value: ProtExample
        description: 'Name of study, will define the output file names'
        key: '--Experiment'
        key_value_separator: ' '
        required: true
        type: text
    -
        default_value: '3'
        description: >-
            Number of replicates per condition (needs to be the same for all,
            add empty columns if necessary)
        key: '--NumReps'
        key_value_separator: ' '
        required: true
        type: number
    -
        default_value: '4'
        description: >-
            Number of different experimental conditions (e.g. time points).
            Minimum: 3
        key: '--NumCond'
        key_value_separator: ' '
        required: true
        type: number
    -
        default_value: 'false'
        description: Paired or unpaired statistical tests
        key: '--paired'
        key_value_separator: ' '
        required: true
        type: radio
        options:
            Paired: 'true'
            Unpaired: 'false'
    -
        default_value: 'true'
        description: >-
            Is the dataset not given with replicates but with individual
            standard deviations in last column?
        key: '--stat'
        key_value_separator: ' '
        required: true
        type: radio
        options:
            'No': 'true'
            'Yes': 'false'
    -
        default_value: 'false'
        description: >-
            File has second column with identifiers (do not need to be unique
            for each feature)
        key: '--secondcol'
        key_value_separator: ' '
        required: true
        type: toggle
        options:
            'off': 'false'
            'on': 'true'
    -
        default_value: 'true'
        description: File contains one-line header?
        key: '--is_header'
        key_value_separator: ' '
        required: true
        type: toggle
        options:
            'off': 'false'
            'on': 'true'
    -
        default_value: '0'
        description: >-
            PreSetNumClustVSClust. If 0, then automatically calculate the
            cluster number from Minimum Centroid Distance.
        key: '--PreSetNumClustVSClust'
        key_value_separator: ' '
        required: true
        type: number

    -
        default_value: '0'
        description: >-
            PreSetNumClustStand. If 0, then automatically calculate the
            cluster number from Minimum Centroid Distance.
        key: '--PreSetNumClustStand'
        key_value_separator: ' '
        required: true
        type: number

    -
        default_value: '20'
        description: >-
            Maximum number of clusters when estimating the number of clusters.
        key: '--maxClust'
        key_value_separator: ' '
        required: true
        type: number

output_type: markdown