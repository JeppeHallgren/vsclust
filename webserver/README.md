**Welcome to the repository of VSClust**
developed at the

[Protein Research Group](http://www.sdu.dk/en/Om_SDU/Institutter_centre/Bmb_biokemi_og_molekylaer_biologi/Forskning/Forskningsgrupper/Protein.aspx)  
Department of Biochemistry and Molecular Biology  
[University of Southern Denmark](http://www.sdu.dk)  

## Citation
When using VSClust, please cite our paper:  
Veit Schwämmle, Ole N Jensen; VSClust: Feature-based variance-sensitive clustering of omics data, Bioinformatics, , bty224, https://doi.org/10.1093/bioinformatics/bty224

## Contact
For software issues and general questions, please submit an issue at [https://bitbucket.org/veitveit/vsclust](https://bitbucket.org/veitveit/vsclust).

## License
GPL-2 or higher

